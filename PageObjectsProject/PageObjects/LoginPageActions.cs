﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UtilityClasses.UiUtilities.UiActions;

namespace PageObjectsProject.PageObjects
{
    public partial class LoginPage
    {
        public static void GoToPage(IWebDriver driver)
        {
            GoToUrl(driver,sourceUrl);
        }

        public static void EnterUserName(IWebDriver driver,string userName)
        {
            SendKeys(driver,UserNameTextBox,userName);
        }

        public static void EnterPassword(IWebDriver driver,string password)
        {
            SendKeys(driver, UserNameTextBox,password);
        }

        public static void ClickLoginButton(IWebDriver driver)
        {
            Click(driver,LoginButton);
        }

        public static void ClickNewUserButton(IWebDriver driver)
        {
            Click(driver,NewUserButton);
        }

    }
}
