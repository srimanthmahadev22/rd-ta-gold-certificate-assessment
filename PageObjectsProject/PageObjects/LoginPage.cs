﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityClasses;
using static UtilityClasses.UiUtilities.UiActions;

namespace PageObjectsProject.PageObjects
{
    public partial class LoginPage
    {
        public static string sourceUrl = "https://demoqa.com/login";
        public static By UserNameTextBox = By.Id("userName");
        public static By PasswordTextBox = By.Id("password");
        public static By LoginButton = By.Id("login");
        public static By NewUserButton = By.Id("newUser");
    }
}
