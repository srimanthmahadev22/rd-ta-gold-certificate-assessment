﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectsProject.PageObjects
{
    public partial class BooksPage
    {
        public static By BooksNames = By.XPath("//*[@class=\"mr-2\"]/a");
        public static By AuthorNames = By.XPath("//div[@class='rt-tr-group']/div/div[3]");
        public static By PublisherNames = By.XPath("//div[@class='rt-tr-group']/div/div[4]");

    }

    public partial class BooksPage
    {
        public static dynamic GetBooksNames(IWebDriver driver)
        {
            return driver.FindElements(BooksNames).Select(n=>n.Text);
        }
        public static dynamic GetAuthorsNames(IWebDriver driver)
        {
            return driver.FindElements(AuthorNames).Select(n=>n.Text);
        }public static dynamic GetPublisherNames(IWebDriver driver)
        {
            return driver.FindElements(PublisherNames).Select(n=>n.Text);
        }
    }
}
