﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectsProject.DataObjects
{
    public class LoginDataObject
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
