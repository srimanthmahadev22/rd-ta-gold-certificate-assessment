﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectsProject.DataObjects
{
    public class LoginResponseBodyObject
    {
        public string userID { get; set; }
        public string username { get; set; }
        public object[] books { get; set; }
    }
}
