﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;

namespace UtilityClasses
{
    public class ExtentReport
    {
        public static readonly string dir = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string testResultPath = dir.Replace("bin\\Debug\\net6.0", "TestResults");

        public static ExtentReports? ExtentReports { get; set; }
        public static ExtentTest? Feature { get; set; }
        public static ExtentTest? Scenario { get; set; }

        public static void ExtentReportSetUp()
        {
            var htmlReporter = GetHtmlReporter();
            ExtentReports = new ExtentReports();
            ExtentReports.AttachReporter(htmlReporter);
            ExtentReports.AddSystemInfo("Application", "Book Store Automation");
            ExtentReports.AddSystemInfo("OS", "Windows");
            ExtentReports.AddSystemInfo("Browser", "Chrome");
        }

        public static ExtentHtmlReporter GetHtmlReporter()
        {
            var htmlReporter = new ExtentHtmlReporter(testResultPath);
            htmlReporter.Config.ReportName = "Automation Status Report";
            htmlReporter.Config.DocumentTitle = "Automation Status Report";
            htmlReporter.Config.Theme = Theme.Standard;
            htmlReporter.Start();
            return htmlReporter;
        }

        public static void ExtentReportTearDown() => ExtentReports?.Flush();
    }
}