﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClasses.WebDriverExtensions
{
    public static class WindowExtensions
    {
        public static void MaximizeWindow(this IWebDriver driver)
        {
            driver.Manage().Window.Maximize();
        }
        
        public static void MiniimizeWindow(this IWebDriver driver)
        {
            driver.Manage().Window.Minimize();
        }

        public static void MakeItFullScreen(this IWebDriver driver)
        {
            driver.Manage().Window.FullScreen();
        }
    }
}
