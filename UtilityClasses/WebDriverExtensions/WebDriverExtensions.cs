﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClasses.WebDriverExtensions
{
    public static class WebDriverExtensions
    {
        public static void TakeScreenshot(this IWebDriver driver, string name)
        {
            string filePath = Path.Combine(@"C:\Users\Srimanth_Kancharla\source\repos\AutomationFramework\ScreenShots\", name);
            var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile(filePath, ScreenshotImageFormat.Png);
        }

        public static void ScrollToElement(this IWebDriver driver, IWebElement element)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

    }
}
