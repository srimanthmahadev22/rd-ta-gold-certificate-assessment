﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClasses.WebDriverExtensions
{
    public static class NavigationExtensions
    {
        static WebDriverManager webDriverManager = new WebDriverManager();
        static IWebDriver driver = webDriverManager.GetDriver(BrowserType.Chrome);

        public static void MoveForward(this IWebDriver driver)
        {
            driver.Navigate().Forward();
        }
        
        public static void GoBack(this IWebDriver driver)
        {
            driver.Navigate().Back();
        }
        
        public static void RefreshPage(this IWebDriver driver)
        {
            driver.Navigate().Refresh();
        }


    }
}
