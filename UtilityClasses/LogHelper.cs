﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;

namespace UtilityClasses
{
    public class LogHelper
    {
        private static ConsoleAppender _consoleAppender;
        private static FileAppender _fileAppender;
        private static string _layout = JsonHandler.GetValueByKey("LogLayout");
       
        private static ILog? _logger;
        private static LogHelper? _instance;
        
        private LogHelper() { }

        public static LogHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogHelper();
                }
                return _instance;
            }
        }

        public ILog GetLogger(Type type)
        {
            if (_consoleAppender == null)
            {
                _consoleAppender = GetConsoleAppender();
            }
            if (_fileAppender == null)
            {
                _fileAppender = GetFileAppender();
            }

            if (_logger != null)
            {
                return _logger;
            }
            else
            {
                _logger = LogManager.GetLogger(type);
                BasicConfigurator.Configure(_consoleAppender, _fileAppender);
                return _logger;
            }
        }

        private ConsoleAppender GetConsoleAppender()
        {
            var consoleAppender = new ConsoleAppender()
            {
                Name = "ConsoleAppender",
                Layout = GetPatternLayout(),
                Threshold = Level.All,
            };
            consoleAppender.ActivateOptions();
            return consoleAppender;
        }

        private FileAppender GetFileAppender()
        {
            var fileAppender = new FileAppender()
            {
                Name = "fileAppender",
                Layout = GetPatternLayout(),
                Threshold = Level.All,
                AppendToFile = true,
                File = JsonHandler.GetValueByKey("LogFilePath")
            };
            fileAppender.ActivateOptions();
            return fileAppender;
        }

        private PatternLayout GetPatternLayout()
        {
            var patternLayout = new PatternLayout()
            {
                ConversionPattern = _layout
            };
            patternLayout.ActivateOptions();
            return patternLayout;
        }
    }
}