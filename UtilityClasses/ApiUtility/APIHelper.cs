﻿using Newtonsoft.Json;
using RestSharp;

namespace ApiUtility

{
    public class APIHelper
    {
        public string baseUrl = "https://demoqa.com/";

        public RestClient SetEndPoint(string endpoint)
        {
            var url = Path.Combine(baseUrl, endpoint);
            var restClient = new RestClient(url);
            return restClient;
        }

        public RestRequest CreateGetRequest()
        {
            var restRequest = new RestRequest();
            restRequest.Method = Method.Get;
            restRequest.AddHeader("Accept", "application/json");
            return restRequest;
        }

        public RestRequest CreatePostRequest(string body) 
        {
            var restRequest = new RestRequest();
            restRequest.Method = Method.Post;
            restRequest.AddHeader("Accept", "application/json");
            restRequest.AddParameter("application/json", body, ParameterType.RequestBody);
            return restRequest;
        }

        public RestResponse GetResponse(RestClient client,RestRequest request) {
            return client.Execute(request);
        }

        public T GetContent<T>(RestResponse response)
        {
            var content = response.Content;
            T dataObject = JsonConvert.DeserializeObject<T>(content);
            return dataObject;
        }

        public string Serialize(dynamic content)
        {
            string serializeObject = JsonConvert.SerializeObject(content,Formatting.Indented);
            return serializeObject;
        }
    }
}