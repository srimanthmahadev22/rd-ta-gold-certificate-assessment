﻿using ApiUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClasses.ApiUtility
{
    public class ApiUtility
    {
        public static T CreateUser<T>(string endpoint, dynamic body)
        {
            var user = new APIHelper();
            var url = user.SetEndPoint(endpoint);
            var request = user.CreatePostRequest(body);
            var response = user.GetResponse(url, request);
            T content = user.GetContent<T>(response);
            return content;
        }
    }
}
