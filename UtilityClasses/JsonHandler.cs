﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClasses
{
    public class JsonHandler
    {
        public static string FilePath = @"C:\Users\Srimanth_Kancharla\source\repos\UtilityClasses\Configurations.json";

        public static string? GetValueByKey(string key)
        {
            string json = File.ReadAllText(FilePath);
            return GetValueByKey(json,key);
        }

        public static string? GetValueByKey(string json, string key)
        {
            JObject jsonObject = JObject.Parse(json);
            JToken token = jsonObject.SelectToken(key);
            if (token != null)
            {
                return token.ToString();
            }
            return null;
        }
    }
}