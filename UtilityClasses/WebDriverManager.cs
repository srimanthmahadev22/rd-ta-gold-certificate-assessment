﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;

namespace UtilityClasses
{
    public enum BrowserType
    {
        Chrome,
        FireFox,
        Edge
    }

    public class WebDriverManager
    {
        private IWebDriver? _driver;

        public IWebDriver GetDriver(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                    _driver = new ChromeDriver();
                    break;

                case BrowserType.FireFox:
                    _driver = new FirefoxDriver();
                    break;

                case BrowserType.Edge:
                    _driver = new EdgeDriver();
                    break;
            }
            return _driver;
        }
    }
}