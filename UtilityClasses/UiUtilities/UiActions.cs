﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace UtilityClasses.UiUtilities
{
    public class UiActions
    {
        private static ILog logger = LogHelper.Instance.GetLogger(typeof(UiActions));

        public static void GoToUrl(IWebDriver driver, string url)
        {
            try
            {
                driver.Navigate().GoToUrl(url);
                logger.Info("Navigated to url sucessfully");
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
        }

        public static void SendKeys(IWebDriver driver, IWebElement element, string keys)
        {
            try
            {
                element.SendKeys(keys);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
        }

        public static void SendKeys(IWebDriver driver, By locator, string keys)
        {
            try
            {
                driver.FindElement(locator).SendKeys(keys);
                logger.Info("Keys sent to element");
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
        }

        public static void Click(IWebDriver driver, By locator)
        {
            try
            {
                driver.FindElement(locator).Click();
                logger.Info("element clicked");

            }
            catch (Exception e)
            {
                logger.Error("clickDIDnt happen");    
                logger.Error(e);
            }
        }


        public static string GetTextOfElement(IWebDriver driver, By locator)
        {
            try
            {
                return driver.FindElement(locator).Text;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return "";
            }
        }

        public static IWebElement FindElementById(IWebDriver driver, string id)
        {
            try
            {
                return driver.FindElement(By.Id(id));
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return null;
            }
        }

        public static void MoveToPageBottom(IWebDriver driver)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,document.body.scrollHeight)");
        }
    }
}