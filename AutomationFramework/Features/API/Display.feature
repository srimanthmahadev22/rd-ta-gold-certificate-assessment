﻿Feature: Display

When user navigates to books page 
then user should be able to view all books in store

@DisplayTestAPI
Scenario Outline: Verifying the books displayed by api get request on books page are flawless
	Given I have the endpoint "<booksEndPoint>"
	When I make a get API request
	Then I have to store the response body in data object class
Examples:
	| booksEndPoint                         |
	| https://demoqa.com/BookStore/v1/Books |

@DisplayTestUI
Scenario Outline: verifying the books displayed in UI is same as books displayed by api
	Given I navigate to "<booksPage>"
	When I store all the book names
	And I store all the respective authors
	And I store all the publishers
	Then I verify book name
	And Then I verify authors
	And Then I verify publishers

Examples:
	| booksPage                |
	| https://demoqa.com/books |
