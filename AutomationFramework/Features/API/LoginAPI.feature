﻿Feature: LoginAPi

Validating the response and status code recieved upon 
using post method with a request body

@LoginValidation
Scenario Outline: Validating the login feature through API
	Given I have a endpoint url "<Url>"
	When I Create login body using the username as "<userName>"
	And password as "<password>" and make a post request
	Then I verify the status code be "<statusCode>"
	And then i verify response body's username be "<userName>"
Examples:
	| Url                                | userName | password     | statusCode |
	| https://demoqa.com/Account/v1/User | sampath  | M@h1ma-J@5t1 | Created    |
	| https://demoqa.com/Account/v1/User | srija    | M@h1ka-J@5t1 | Created    |
	| https://demoqa.com/Account/v1/User | rohit    | M@p1ma-J@5t1 | Created    |
	| https://demoqa.com/Account/v1/User | ramesh   | M@h1m@-J@5t1 | Created    |
	| https://demoqa.com/Account/v1/User | mahesh   | M@h1ma-J@5T1 | Created    |
	