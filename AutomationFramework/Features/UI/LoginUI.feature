﻿Feature: LoginUI

User interface for login.
By entering user name and password user can login to website.

@LoginValidation
Scenario Outline: To test the login mechanism of UI for a specific credentials
	Given User navigates to "<loginPage>"
	When User inputs username as "<userName>"
	And User inputs password as "<password>"
	And User clicks on login button
	Then I verify the "<userName>" on the dashboard of Homepage
Examples:
	| loginPage                | userName | password     |
	| https://demoqa.com/login | sampath  | M@h1ma-J@5t1 |
	| https://demoqa.com/login | srija    | M@h1ka-J@5t1 |
	| https://demoqa.com/login | rohit    | M@p1ma-J@5t1 |
	| https://demoqa.com/login | ramesh   | M@h1m@-J@5t1 |
	| https://demoqa.com/login | mahesh   | M@h1ma-J@5T1 |
