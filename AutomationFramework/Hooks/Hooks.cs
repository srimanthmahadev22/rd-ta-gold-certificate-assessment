﻿using AventStack.ExtentReports.Gherkin.Model;
using BoDi;
using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using UtilityClasses;
using UtilityClasses.WebDriverExtensions;

namespace AutomationFramework.Hooks
{
    [Binding]
    public sealed class Hooks : ExtentReport
    {

        private readonly IObjectContainer _container;
        private static ILog logger = LogHelper.Instance.GetLogger(typeof(Hooks));
        //private static ILog logger = LogHelper.GetLogger(typeof(AmazonAutomationHooks));

        public Hooks(IObjectContainer container)
        {
            _container = container;
        }

        [BeforeScenario]
        public void BeforeScenario(ScenarioContext scenarioContext)
        {
            WebDriverManager webDriverManager = new WebDriverManager();
            IWebDriver driver = webDriverManager.GetDriver(BrowserType.Chrome);
            driver.MaximizeWindow();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            _container.RegisterInstanceAs<IWebDriver>(driver);
            Scenario = Feature.CreateNode<Scenario>(scenarioContext.ScenarioInfo.Title);
        }


        [AfterScenario]
        public void AfterScenario()
        {
            var driver = _container.Resolve<IWebDriver>();
            if (driver != null)
            {
                driver.Quit();
            }
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            logger.Info("Running before the test");
            ExtentReportSetUp();
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            logger.Info("Running after the test run");
            ExtentReportTearDown();
        }

        [BeforeFeature]
        public static void BeforeFeature(FeatureContext featureContext)
        {
            logger.Info("Running Before Feature");
            Feature = ExtentReports.CreateTest<Feature>(featureContext.FeatureInfo.Title);
        }

        [AfterStep]
        public static void AfterStep(ScenarioContext scenarioContext)
        {
            logger.Info("Running after the step..");
            string stepType = scenarioContext.StepContext.StepInfo.StepDefinitionType.ToString();
            string stepName = scenarioContext.StepContext.StepInfo.Text;

            //when passes
            if (scenarioContext.TestError == null)
            {
                if (stepType == "Given")
                {
                    Scenario.CreateNode<Given>(stepName);
                }
                else if (stepType == "When")
                {
                    Scenario.CreateNode<When>(stepName);
                }
                else if (stepType == "Then")
                {
                    Scenario.CreateNode<Then>(stepName);
                }
            }

            //when scenario fails
            if (scenarioContext.TestError != null)
            {
                if (stepType == "Given")
                {
                    Scenario.CreateNode<Given>(stepName).Fail(scenarioContext.TestError.Message);
                }
                else if (stepType == "When")
                {
                    Scenario.CreateNode<When>(stepName).Fail(scenarioContext.TestError.Message);
                }
                else if (stepType == "Then")
                {
                    Scenario.CreateNode<Then>(stepName).Fail(scenarioContext.TestError.Message);
                }
            }
        }
    }
}