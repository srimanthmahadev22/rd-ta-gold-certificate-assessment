using ApiUtility;
using log4net;
using log4net.Repository.Hierarchy;
using NUnit.Framework;
using PageObjectsProject.DataObjects;
using RestSharp;
using System;
using System.Net;
using TechTalk.SpecFlow;
using UtilityClasses;

namespace AutomationFramework.StepDefinitions
{
    [Binding]
    public class LoginAPiStepDefinitions
    {
        RestClient restClient;
        RestRequest request;
        RestResponse response;
        APIHelper apiHelper = new APIHelper();
        LoginDataObject loginDataObject = new LoginDataObject();

        [Given(@"I have a endpoint url ""([^""]*)""")]
        public void GivenIHaveAEndpointUrl(string baseUrl)
        {
            restClient = new RestClient(baseUrl);
        }

        [When(@"I Create login body using the username as ""([^""]*)""")]
        public void WhenICreateLoginBodyUsingTheUsernameAs(string userName)
        {
            loginDataObject.userName= userName;
        }

        [When(@"password as ""([^""]*)"" and make a post request")]
        public void WhenPasswordAsAndMakeAPostRequest(string password)
        {
            loginDataObject.password= password;
            string body = apiHelper.Serialize(loginDataObject);
            request = apiHelper.CreatePostRequest(body);
        }


        [Then(@"I verify the status code be ""([^""]*)""")]
        public void ThenIVerifyTheStatusCodeBe(string statusCode)
        {
            response = restClient.Execute(request);
            Assert.That(statusCode, Is.EqualTo((response.StatusCode.ToString())));
        }

        [Then(@"then i verify response body's username be ""([^""]*)""")]
        public void ThenThenIVerifyResponseBodysUsernameBe(string userName)
        {
            LoginResponseBodyObject loginResponseBodyObject = apiHelper.GetContent<LoginResponseBodyObject>(response);
            Assert.That(userName, Is.EqualTo(loginResponseBodyObject.username));
        }
    }
}
