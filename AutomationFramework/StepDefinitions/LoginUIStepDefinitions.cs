using NUnit.Framework;
using OpenQA.Selenium;
using PageObjectsProject.PageObjects;
using static UtilityClasses.UiUtilities.UiActions;

namespace AutomationFramework.StepDefinitions
{
    [Binding]
    public class LoginUIStepDefinitions
    {
        IWebDriver driver;
        public LoginUIStepDefinitions(IWebDriver driver)
        {
            this.driver = driver;
        }

        [Given(@"User navigates to ""([^""]*)""")]
        public void GivenUserNavigatesTo(string url)
        {
            GoToUrl(driver,url);
        }

        [When(@"User inputs username as ""([^""]*)""")]
        public void WhenUserInputsUsernameAs(string userName)
        {
            SendKeys(driver, LoginPage.UserNameTextBox,userName + Keys.Tab);
        }

        [When(@"User inputs password as ""([^""]*)""")]
        public void WhenUserInputsPasswordAs(string password)
        {
            SendKeys(driver, LoginPage.PasswordTextBox, password + Keys.Tab);
        }

        [When(@"User clicks on login button")]
        public void WhenUserClicksOnLoginButton()
        {
            MoveToPageBottom(driver);
            Click(driver, LoginPage.LoginButton);
        }

        [Then(@"I verify the ""([^""]*)"" on the dashboard of Homepage")]
        public void ThenIVerifyTheOnTheDashboardOfHomepage(string expectedUserName)
        {
            string pageUserName = driver.FindElement(HomePage.UserName).Text;
            Assert.That(pageUserName, Is.EqualTo(expectedUserName));
        }
    }
}
