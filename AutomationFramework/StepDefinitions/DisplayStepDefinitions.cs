using ApiUtility;
using log4net;
using OpenQA.Selenium;
using PageObjectsProject.DataObjects;
using RestSharp;
using System;
using TechTalk.SpecFlow;
using UtilityClasses;
using UtilityClasses.UiUtilities;
using Newtonsoft.Json;
using NUnit.Framework;
using PageObjectsProject.PageObjects;

namespace AutomationFramework.StepDefinitions
{
    [Binding]
    public class DisplayStepDefinitions
    {
        ILog logger = LogHelper.Instance.GetLogger(typeof(DisplayStepDefinitions));

        IWebDriver driver;
        APIHelper aPIHelper = new APIHelper();
        RestClient client;
        RestRequest request;
        RestResponse response;
        static Book[] books;
        dynamic bookNames;
        dynamic authorNames;
        dynamic publisherNames;


        DisplayStepDefinitions(IWebDriver driver)
        {
            this.driver = driver;
        }

        [Given(@"I have the endpoint ""([^""]*)""")]
        public void GivenIHaveTheEndpoint(string url)
        {
            client = new RestClient(url);
        }

        [When(@"I make a get API request")]
        public void WhenIMakeAGetAPIRequest()
        {
            request = aPIHelper.CreateGetRequest();
            response = aPIHelper.GetResponse(client, request);
        }

        [Then(@"I have to store the response body in data object class")]
        public void ThenIHaveToStoreTheResponseBodyInDataObjectClass()
        {
            BookObjects apiBookObjects = JsonConvert.DeserializeObject<BookObjects>(response.Content.ToString());
            books = apiBookObjects.books;
        }

        [Given(@"I navigate to ""([^""]*)""")]
        public void GivenINavigateTo(string url)
        {
            UiActions.GoToUrl(driver, url);
        }

        [When(@"I store all the book names")]
        public void WhenIStoreAllTheBookNames()
        {
            bookNames = BooksPage.GetBooksNames(driver);
        }

        [When(@"I store all the respective authors")]
        public void WhenIStoreAllTheRespectiveAuthors()
        {
            authorNames = BooksPage.GetAuthorsNames(driver);
        }

        [When(@"I store all the publishers")]
        public void WhenIStoreAllThePublishers()
        {
            publisherNames = BooksPage.GetPublisherNames(driver);
        }


        [Then(@"I verify book name")]
        public void ThenIVerifyBookName()
        {
            int i = 0;
            foreach (var book in bookNames)
            {
                Assert.That(book.ToString(), Is.EqualTo(books[i].title.ToString()));
                i++;
                if (i == books.Count())
                {
                    break;
                }
            }
        }

        [Then(@"Then I verify authors")]
        public void ThenThenIVerifyAuthors()
        {

            int i = 0;
            foreach (var author in authorNames)
            {
                Assert.That(author.ToString(), Is.EqualTo(books[i].author.ToString()));
                i++;
                if (i == books.Count())
                {
                    break;
                }
            }
        }

        [Then(@"Then I verify publishers")]
        public void ThenThenIVerifyPublishers()
        {
            int i = 0;
            foreach (var publisher in publisherNames)
            {
                Assert.That(publisher.ToString(), Is.EqualTo(books[i].publisher.ToString()));
                i++;
                if (i == books.Count())
                {
                    break;
                }
            }

        }

    }
}
